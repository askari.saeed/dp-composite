package com.company;

import java.util.ArrayList;

class ListComposite extends ListComponent{

    private final ArrayList<ListComponent> children = new ArrayList<>();

    @Override
    void printValue(){
        System.out.print("( ");
        for (int i=children.size()-1; i>=0 ; --i){
            children.get(i).printValue();
        }
        System.out.print(") ");
    }

    @Override
    void addChild(ListComponent component){
        children.add(component);
    }

}
