package com.company;

class Item extends ListComponent{

    private int value;

    Item(int v){
        value = v;
    }

    @Override
    void printValue(){
        System.out.print(value + " ");
    }

}
