package com.company;

import java.util.Stack;

class ListBuilder {
    private ListComponent root;
    private final Stack<ListComponent> stack = new Stack<>();
    void buildOpenBracket() {
        ListComposite composite = new ListComposite();
        if (root == null){
            stack.push(composite);
            root = composite;
            return;
        }
        stack.peek().addChild(composite);
        stack.push(composite);
    }

    void buildCloseBracket() {
        stack.pop();
    }

    void buildElement(String token) {
        stack.peek().addChild(new Item(Integer.valueOf(token)));
    }

    ListComponent getList() {
        return root;
    }
}
