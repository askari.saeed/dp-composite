package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ListBuilder builder = new ListBuilder();
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] tokens = line.split(" ");
        for (String token: tokens){
            if (token.equals("(")){
                builder.buildOpenBracket();
            } else if (token.equals(")")){
                builder.buildCloseBracket();
            } else {
                builder.buildElement(token);
            }
        }
        ListComponent list = builder.getList();
        list.printValue();
    }
}
